#!/bin/bash
#	Copyright 2020 Charadon
#
#	Licensed under the Apache License, Version 2.0 (the "License");
#	you may not use this file except in compliance with the License.
#	You may obtain a copy of the License at
#	http://www.apache.org/licenses/LICENSE-2.0
#
#	Unless required by applicable law or agreed to in writing, software
#	distributed under the License is distributed on an "AS IS" BASIS,
#	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#	or implied. See the License for the specific language governing
#	permissions and limitations under the License.

export WEATHER="$(curl wttr.in/?format=1)"
while true ; do
	sleep 0.05
	xsetroot -name "[ﱘ$(playerctl metadata title) - $(playerctl status)] [ $( pamixer --get-volume )%] [易 $( awk '/^Swap/ {print $3}' <(free -m -h --si) )/$( awk '/^Swap/ {print $2}' <(free -m -h --si) )] [ $( awk '/^Mem/ {print $3}' <(free -m -h --si) )/$( awk '/^Mem/ {print $2}' <(free -m -h --si) )] [ $(date "+%a %x, %r")]"
	if ! pgrep dwm > /dev/null ; then
		break
	fi
done 
